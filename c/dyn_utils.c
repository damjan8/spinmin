//
// Created by Aleksei Ivanov on 2019-02-25.
//

#include <math.h>

void cross(double *x, double *y, double *out);


double calc_det3(double x1, double x2, double x3,
                 double y1, double y2, double y3,
                 double z1, double z2, double z3){

    return x1 * (y2 * z3 - y3 * z2) -
           x2 * (y1 * z3 - y3 * z1) +
           x3 * (y1 * z2 - y2 * z1);

}


void sigma_x_y(double *x, double *y,
               double D,double alpha,
               double h, int damping_only, double *out){

    double temp[3];
    double c = alpha * sqrt(2.0 * D / h);
    int i;

    cross(x, y, temp);

    for (i = 0; i < 3; i++){
        temp[i] *= c;
    }
    for (i = 0; i < 3; i++){
        out[i] += temp[i];
    }
    if (damping_only == 0){
        for (i = 0; i < 3; i++){
            out[i] += y[i] * sqrt(2.0 * D / h);
        }
    }

}


void calc_linear_equations(double *magn_mom, double *b, double dt, double *out){

    double rhs[3];
    int i;

    double d_mat[3][3] = {{1.0,  0.5 * dt * b[2], -0.5 * dt * b[1]},
                          {-0.5 * dt * b[2], 1.0, 0.5 * dt * b[0]},
                          {0.5 * dt * b[1], -0.5 * dt * b[0], 1.0}};

    double det = 1.0 + d_mat[1][2] * d_mat[1][2]
                     + d_mat[0][2] * d_mat[0][2] +
                       d_mat[0][1] * d_mat[0][1];

    cross(magn_mom, b, rhs);
    for (i = 0; i < 3; i++){
        rhs[i] = magn_mom[i] - rhs[i] * dt * 0.5;
    }

    double detdx = calc_det3(
            rhs[0], d_mat[0][1], d_mat[0][2],
            rhs[1], d_mat[1][1], d_mat[1][2],
            rhs[2], d_mat[2][1], d_mat[2][2]
            );

    double detdy = calc_det3(
            d_mat[0][0], rhs[0], d_mat[0][2],
            d_mat[1][0], rhs[1], d_mat[1][2],
            d_mat[2][0], rhs[2], d_mat[2][2]
            );

    double detdz = calc_det3(
            d_mat[0][0], d_mat[0][1], rhs[0],
            d_mat[1][0], d_mat[1][1], rhs[1],
            d_mat[2][0], d_mat[2][1], rhs[2]
            );

    out[0] = detdx / det;
    out[1] = detdy / det;
    out[2] = detdz / det;

}