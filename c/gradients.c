#include <Python.h>
#include <numpy/arrayobject.h>
#include <math.h>
#define DOUBLEP(a) ((double*)PyArray_DATA(a))
#define INTP(a) ((int*)PyArray_DATA(a))
// #define LINTP(a) ((long int*)PyArray_DATA(a))

double dot3(const double *x, const double *y);
void cross(const double *x, const double *y, double *out);
void calc_diff(double *x, double *y, double *out);
void get_offset_positions(double *x, double *offset,
                          double *cell, double *out);
void normalize(double *x);
int get_incriment(int k);


static PyObject* xgrad(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    PyArrayObject *grad;
    PyArrayObject *nbl1;
    PyArrayObject *nbl2;
    double J;

    if (!PyArg_ParseTuple(args, "DOOOO", &J, &spins, &nbl1, &nbl2, &grad))
        return NULL;

    double* pspin = DOUBLEP(spins);
    double* pgrad = DOUBLEP(grad);

    int itemsize_nbl1 = PyArray_ITEMSIZE(nbl1);
    int incr = get_incriment(itemsize_nbl1);
    int* n1 = INTP(nbl1);
    int* n2 = INTP(nbl2);
    int k;

    int i1;
    int i2;
    int l;


    for(k = 0; k < PyArray_DIMS(nbl1)[0]; k++){
        i1 = n1[k * incr];
        i2 = n2[k * incr];
        for(l = 0; l < 3; l++){
            *(pgrad + 3 * i1 + l) -=
                *(pspin + i2 * 3 + l) * J;
        }
    }

    Py_INCREF(Py_None);
    return Py_None;
}


static PyObject* dmgrad(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    PyArrayObject *grad;
    PyArrayObject *nbl1;
    PyArrayObject *nbl2;
    PyArrayObject *positions;
    PyArrayObject *offsets;
    PyArrayObject *cell;
    double D;
    int bloch = 0;
    PyArrayObject *NeelVec;

    if (!PyArg_ParseTuple(
            args, "DOOOOOOOOI", &D, &spins, &nbl1, &nbl2,
            &positions, &offsets, &cell, &grad, &NeelVec, &bloch))

        return NULL;

    double* pspin = DOUBLEP(spins);
    double* ppos = DOUBLEP(positions);
    double* pgrad = DOUBLEP(grad);

    int itemsize_nbl1 = PyArray_ITEMSIZE(nbl1);
    int incr = get_incriment(itemsize_nbl1);
    int* n1 = INTP(nbl1);
    int* n2 = INTP(nbl2);
    double* poffs2 = DOUBLEP(offsets);
    double* pcell = DOUBLEP(cell);
    double* pnvec = DOUBLEP(NeelVec);

    double cross_prod[3] = {0.0, 0.0, 0.0};
    double r_ij[3] = {0.0, 0.0, 0.0};
    double offset_pos[3] = {0.0, 0.0, 0.0};
    double dmv[3] = {0.0, 0.0, 0.0};

    int k;
    int i1;
    int i2;
    int l;

    for(k = 0; k < PyArray_DIMS(nbl1)[0]; k++){
        i1 = n1[k * incr];
        i2 = n2[k * incr];
        get_offset_positions(ppos + 3 * i2, poffs2 + 3 * k,
                             pcell, offset_pos);
        calc_diff(ppos + 3 * i1, offset_pos, r_ij);
        normalize(r_ij);

        if (bloch == 0)
            cross(pspin + 3 * i2, r_ij, cross_prod);
        else{
            cross(r_ij, pnvec, dmv);
            cross(pspin + 3 * i2, dmv, cross_prod);
        }

        for(l = 0; l < 3; l++){
            *(pgrad + 3 * i1 + l) -=
                cross_prod[l] * D;
        }
    }

    Py_INCREF(Py_None);
    return Py_None;
}


static PyObject* angrad(PyObject* self, PyObject* args){

    PyArrayObject *grad;
    PyArrayObject *spins;
    double K;
    PyArrayObject *e_an;

    if (!PyArg_ParseTuple(args, "DOOO", &K, &e_an, &spins, &grad))
        return NULL;

    double* pgrad = DOUBLEP(grad);
    double* pspin = DOUBLEP(spins);
    double* pe = DOUBLEP(e_an);
    double dot_prod = 0.0;
    int k;
    int l;

    for(k = 0; k < PyArray_DIMS(spins)[0]; k++){
        dot_prod = dot3(pspin + k * 3, pe);
        for(l = 0; l < 3; l++ ){
            *(pgrad + 3 * k + l) -= *(pe + l)* dot_prod * K * 2.0;
        }
    }

    Py_INCREF(Py_None);
    return Py_None;
}


static PyObject* zgrad(PyObject* self, PyObject* args){

    PyArrayObject *grad;
    double Z;
    PyArrayObject *e_b;

    if (!PyArg_ParseTuple(args, "DOO", &Z, &e_b, &grad))
        return NULL;

    double* pgrad = DOUBLEP(grad);
    double* pe = DOUBLEP(e_b);
    int k;
    int l;

    for(k = 0; k < PyArray_DIMS(grad)[0]; k++){
        for(l = 0; l < 3; l++ ){
            *(pgrad + 3 * k + l) -= *(pe + l) * Z;
        }
    }

    Py_INCREF(Py_None);
    return Py_None;
}


static PyMethodDef GradMethods[] =
{
     {"xgrad", xgrad, METH_VARARGS, "evaluate xgrad"},
     {"dmgrad", dmgrad, METH_VARARGS, "evaluate dmgrad"},
     {"angrad", angrad, METH_VARARGS, "evaluate angrad"},
     {"zgrad", zgrad, METH_VARARGS, "evaluate zgrad"},

     {NULL, NULL, 0, NULL}
};


static struct PyModuleDef cModPyDem =
{
    PyModuleDef_HEAD_INIT,
    "gradient_module", "Some documentation",
    -1,
    GradMethods
};

PyMODINIT_FUNC
PyInit_gradient_module(void)
{
    import_array();
    return PyModule_Create(&cModPyDem);
}
