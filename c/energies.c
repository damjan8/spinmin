#include <Python.h>
#include <numpy/arrayobject.h>
#include <math.h>
#define DOUBLEP(a) ((double*)PyArray_DATA(a))
#define INTP(a) ((int*)PyArray_DATA(a))
// #define LINTP(a) ((long int*)PyArray_DATA(a))

double dot3(const double *x, const double *y);
void cross(const double *x, const double *y, double *out);
void calc_diff(double *x, double *y, double *out);
void get_offset_positions(double *x, double *offset,
                          double *cell, double *out);
void normalize(double *x);
int get_incriment(int k);

static PyObject* xenergy(PyObject* self, PyObject* args){
	
    PyArrayObject *spins;
    PyArrayObject *nbl1;
    PyArrayObject *nbl2;
    double J;

    if (!PyArg_ParseTuple(args, "DOOO", &J, &spins, &nbl1, &nbl2))
        return NULL;

    double* pspin = DOUBLEP(spins);

    int itemsize_nbl1 = PyArray_ITEMSIZE(nbl1);
    int incr = get_incriment(itemsize_nbl1);
    int* n1 = INTP(nbl1);
    int* n2 = INTP(nbl2);
    int k;

    double energy = 0.0;
    int i1;
    int i2;

    for(k = 0; k < PyArray_DIMS(nbl1)[0]; k++){
        i1 = n1[k * incr];
        i2 = n2[k * incr];
        energy += dot3(pspin + i1 * 3, pspin + i2 * 3);
    }
	energy *= -1.0 * J;

	return Py_BuildValue("d", energy);
}


static PyObject* dmenergy(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    PyArrayObject *nbl1;
    PyArrayObject *nbl2;
    PyArrayObject *positions;
    PyArrayObject *offsets;
    PyArrayObject *cell;
    double D;
    int bloch = 0;
    PyArrayObject *NeelVec;

    if (!PyArg_ParseTuple(
            args, "DOOOOOOOI", &D, &spins, &nbl1, &nbl2,
            &positions, &offsets, &cell, &NeelVec, &bloch))

        return NULL;

    double* pspin = DOUBLEP(spins);
    double* ppos = DOUBLEP(positions);

    int itemsize_nbl1 = PyArray_ITEMSIZE(nbl1);
    int incr = get_incriment(itemsize_nbl1);
    int* n1 = INTP(nbl1);
    int* n2 = INTP(nbl2);
    double* poffs2 = DOUBLEP(offsets);
    double* pcell = DOUBLEP(cell);
    double* pnvec = DOUBLEP(NeelVec);
    int k;

    double energy = 0.0;
    int i1;
    int i2;
    double cross_prod[3] = {0.0, 0.0, 0.0};
    double offset_pos[3] = {0.0, 0.0, 0.0};
    double r_ij[3] = {0.0, 0.0, 0.0};

    double dmv[3] = {0.0, 0.0, 0.0};


    for(k = 0; k < PyArray_DIMS(nbl1)[0]; k++){
        i1 = n1[k * incr];
        i2 = n2[k * incr];

        cross(pspin + 3 * i1, pspin + 3 * i2, cross_prod);
        get_offset_positions(ppos + 3 * i2, poffs2 + 3 * k,
                             pcell, offset_pos);
        calc_diff(ppos + 3 * i1, offset_pos, r_ij);
        normalize(r_ij);

        if (bloch == 0)
            energy += dot3(r_ij, cross_prod);
        else{
            cross(r_ij, pnvec, dmv);
            energy += dot3(dmv, cross_prod);
        }


    }
	energy *= -1.0 * D;

	return Py_BuildValue("d", energy);
}


static PyObject* anenergy(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    PyArrayObject *e_an;
    double K;

    if (!PyArg_ParseTuple(args, "DOO", &K, &e_an, &spins))
        return NULL;

    double* pspin = DOUBLEP(spins);
    double* pe = DOUBLEP(e_an);

    int k;
    double energy = 0.0;
    double dot_prod = 0.0;

    for(k = 0; k < PyArray_DIMS(spins)[0]; k++){
        dot_prod = dot3(pspin + k * 3, pe);
        energy +=  dot_prod * dot_prod;
    }
	energy *= -1.0 * K;
	return Py_BuildValue("d", energy);
}


static PyObject* zenergy(PyObject* self, PyObject* args){

    PyArrayObject *spins;
    double Z;
    PyArrayObject *e_b;

    if (!PyArg_ParseTuple(args, "DOO", &Z, &e_b, &spins))
        return NULL;

    double* pspin = DOUBLEP(spins);
    double* pe = DOUBLEP(e_b);
    int k;
    double energy = 0.0;

    for(k = 0; k < PyArray_DIMS(spins)[0]; k++){
        energy += dot3(pspin + k * 3, pe);
    }

	energy *= -1.0 * Z;
	return Py_BuildValue("d", energy);
}


static PyMethodDef EnMethods[] =
{
     {"xenergy", xenergy, METH_VARARGS, "evaluate xenergy"},
     {"dmenergy", dmenergy, METH_VARARGS, "evaluate dmenergy"},
     {"anenergy", anenergy, METH_VARARGS, "evaluate dmenergy"},
     {"zenergy", zenergy, METH_VARARGS, "evaluate dmenergy"},

     {NULL, NULL, 0, NULL}
};


static struct PyModuleDef cModPyDem =
{
    PyModuleDef_HEAD_INIT,
    "energy_module", "Some documentation",
    -1,
    EnMethods
};

PyMODINIT_FUNC
PyInit_energy_module(void)
{
    import_array();
    return PyModule_Create(&cModPyDem);
}
