SpinMin
=======

Package for the energy minimisation and 
the minimum energy path calculation of spin systems.

Requierements
-------------

* Python 3.6-
* ASE [(Atomic Simulation Enviroment)](http://wiki.fysik.dtu.dk/ase)

Installation
------------
First install ASE [(Atomic Simulation Enviroment)](http://wiki.fysik.dtu.dk/ase).
See installation page or do this:

```
$ cd
$ pwd
/user/home
$ git clone https://gitlab.com/ase/ase.git
$ cd ase 
```
Then add to your bashrc (or bash_profile) file the following:
```
export ASE_HOME=~/ase
export PYTHONPATH=${ASE_HOME}:${PYTHONPATH}
export PATH=${ASE_HOME}/tools:${PATH}
```

Now install the SpinMin:
```
$ cd
$ git clone https://gitlab.com/alxvov/spinmin.git
$ cd spinmin
$ python setup.py build_ext
```
Add to your bashrc (or bash_profile):
```
export SPIN_PLATFORM=`python -c "from distutils import util, sysconfig; print(util.get_platform()+'-'+sysconfig.get_python_version())"`
export PYTHONPATH=~/spinmin:$PYTHONPATH
export PYTHONPATH=~/spinmin/build/lib.${SPIN_PLATFORM}:$PYTHONPATH
```

Example
-------
Calculation of skrymionic states.

```python
from ase.build import bcc100
from spinmin.hamiltonian import SpinHamiltonian
from spinmin.minimise.unitary_minimisation import UnitaryOptimisation
from spinmin.utilities import random_spins, plot_xy

atoms = bcc100('Fe', a=2.856, orthogonal=True, size=(20, 20, 1))
spins = random_spins(len(atoms), seed=7)
atoms.set_initial_magnetic_moments(spins)
ham = SpinHamiltonian(atoms, spins,
                      interactions={'J': 10.0,
                                    'DM': 5.0,
                                    'Z_ad': {'ampl': 2.0,
                                             'dir': [0.0, 0.0, 1.0]},
                                    'r_c': 1.45})
opt = UnitaryOptimisation(ham)
opt.run()
plot_xy(atoms, spins)
```

References
---------

Paper about minimisation algorithms: https://arxiv.org/abs/1904.02669. 