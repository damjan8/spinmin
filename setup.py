from distutils.core import setup, Extension
import numpy

# xoptions = ['-Wall', '-Wextra', '-fno-stack-protector']

energy_module = Extension('energy_module',
                          sources=['c/energies.c', 'c/utils.c'],
                          define_macros = [('NPY_NO_DEPRECATED_API', 'NPY_1_7_API_VERSION')],
                          include_dirs=[numpy.get_include()],
#                           extra_compile_args = xoptions
                          )

gradient_module = Extension('gradient_module',
                            sources=['c/gradients.c', 'c/utils.c'],
                            define_macros = [('NPY_NO_DEPRECATED_API', 'NPY_1_7_API_VERSION')],
                            include_dirs=[numpy.get_include()],
#                             extra_compile_args = xoptions
                           )

uo_module = Extension('uo_module',
                      sources=['c/uo.c',
                               'c/uo_utils.c',
                               'c/utils.c'],
                      define_macros = [('NPY_NO_DEPRECATED_API', 'NPY_1_7_API_VERSION')],
                      include_dirs=[numpy.get_include()],
#                      extra_compile_args = xoptions
                      )

dyn_module = Extension('dyn_module',
                        sources=['c/dyn.c',
                                 'c/dyn_utils.c',
                                 'c/utils.c'],
                        define_macros = [('NPY_NO_DEPRECATED_API', 'NPY_1_7_API_VERSION')],
                        include_dirs=[numpy.get_include()],
#                         extra_compile_args = xoptions
                       )


setup(ext_modules=[energy_module, gradient_module,
                   uo_module, dyn_module])

