from ase.neighborlist import NeighborList, NewPrimitiveNeighborList
from spinmin.energy_and_gradients import *
import numpy as np
import energy_module
import gradient_module


class SpinHamiltonian:

    def __init__(self, atoms, spins, interactions=None):

        self.spins = spins
        self.atoms = atoms
        self.type = 'single_image'

        self.interactions = interactions

        self.r_c = [interactions['r_c']] * len(atoms)
        self.J = interactions.get('J')
        self.DM = interactions.get('DM')
        self.K_ad = interactions.get('K_ad')
        self.Z_ad = interactions.get('Z_ad')

        # neighbours
        self.nbl_one_way = NeighborList(self.r_c, skin=0.01,
                                        self_interaction=False,
                                        bothways=False,
                                        primitive=NewPrimitiveNeighborList)

        self.nbl_both_way = NeighborList(self.r_c, skin=0.01,
                                         self_interaction=False,
                                         bothways=True,
                                         primitive=NewPrimitiveNeighborList)

        self.nbl_one_way.update(atoms)
        self.nbl_both_way.update(atoms)

        self.nbl1_ow = []
        self.nbl2_ow = []
        self.offsets_ow = []
        for i in range(len(atoms)):
            nl_indices, offsets = self.nbl_one_way.get_neighbors(i)
            for j, z in zip(nl_indices, offsets):
                self.nbl1_ow.append(i)
                self.nbl2_ow.append(j)
                self.offsets_ow.append(z.tolist())
        self.nbl1_ow = np.array(self.nbl1_ow)
        self.nbl2_ow = np.array(self.nbl2_ow)
        self.offsets_ow = np.asarray(self.offsets_ow, dtype=float)

        self.nbl1_bw = []
        self.nbl2_bw = []
        self.offsets_bw = []
        for i in range(len(atoms)):
            nl_indices, offsets = self.nbl_both_way.get_neighbors(i)
            for j, z in zip(nl_indices, offsets):
                self.nbl1_bw.append(i)
                self.nbl2_bw.append(j)
                self.offsets_bw.append(z.tolist())
        self.nbl1_bw = np.array(self.nbl1_bw)
        self.nbl2_bw = np.array(self.nbl2_bw)
        self.offsets_bw = np.asarray(self.offsets_bw, dtype=float)

        self.energy_heiss = 0.0
        self.energy_dm = 0.0
        self.energy_anis = 0.0
        self.energy_zee = 0.0

        # calculate length of spins and normalize it
        self.mm = []
        for i, s in enumerate(self.spins):
            x = np.sqrt(np.dot(s, s))
            s /= x
            if i > 0:
                if abs(x - self.mm[-1]) > 1.0e-6:
                    raise ValueError('Only spins with equal'
                                     ' length are accepted')
            self.mm.append(x)

    def get_energy(self):

        atoms, spins = self.atoms, self.spins

        if self.J is not None:
            # self.energy_heiss = calc_heis_energy(self.J, spins,
            #                                      self.nbl_one_way)

            self.energy_heiss = energy_module.xenergy(self.J, spins,
                                                      self.nbl1_ow,
                                                      self.nbl2_ow)

        if self.DM is not None:
            # self.energy_dm = calc_dm_energy(self.DM, atoms, spins,
            #                                 self.nbl_one_way)

            if type(self.DM) is float:
                D = self.DM
                bloch = 0
                perp_v = np.array([0.0, 0.0, 0.0])
            else:
                D, perp_v = \
                    self.DM['ampl'], np.asarray(self.DM['dir'])
                bloch = 1

            cell = atoms.get_cell()
            if not isinstance(cell,np.ndarray):
                cell = cell.array
            self.energy_dm = energy_module.dmenergy(D, spins,
                                                    self.nbl1_ow,
                                                    self.nbl2_ow,
                                                    atoms.positions,
                                                    self.offsets_ow,
                                                    cell,
                                                    perp_v, bloch
                                                    )
        if self.K_ad is not None:
            # self.energy_anis = calc_anisotropy_energy(self.K_ad,
            #                                           spins)
            K, e_c = self.K_ad['ampl'], np.asarray(self.K_ad['dir'])
            self.energy_anis = energy_module.anenergy(K, e_c, spins)

        if self.Z_ad is not None:
            # self.energy_zee = calc_zeeman_energy(self.Z_ad, spins)
            Z, e_c = self.Z_ad['ampl'], np.asarray(self.Z_ad['dir'])
            self.energy_zee = energy_module.zenergy(Z, e_c, spins)

        self.e_total = self.energy_heiss + \
                       self.energy_dm + \
                       self.energy_anis + \
                       self.energy_zee

        return self.e_total

    def get_gradients(self):

        atoms, spins = self.atoms, self.spins
        grad = np.zeros(shape=(spins.shape[0], 3))
        if self.J is not None:

            # grad += calc_heis_grad(self.J, spins, self.nbl_both_way)
            gradient_module.xgrad(self.J, spins, self.nbl1_bw,
                                  self.nbl2_bw, grad)

        if self.DM is not None:
            if type(self.DM) is float:
                D = self.DM
                bloch = 0
                perp_v = np.array([0.0, 0.0, 0.0])
            else:
                D, perp_v = \
                    self.DM['ampl'], np.asarray(self.DM['dir'])
                bloch = 1

            # grad += calc_dm_grad(self.DM, atoms, spins,
            #                      self.nbl_both_way)
            cell = atoms.get_cell()
            if not isinstance(cell,np.ndarray):
                cell = cell.array

            gradient_module.dmgrad(D, spins, self.nbl1_bw,
                                   self.nbl2_bw, atoms.positions,
                                   self.offsets_bw, cell,
                                   grad, perp_v, bloch)
        if self.K_ad is not None:
            # grad += calc_anisotropy_grad(self.K_ad, spins)
            K, e_c = self.K_ad['ampl'], np.asarray(self.K_ad['dir'])
            gradient_module.angrad(K, e_c, spins, grad)

        if self.Z_ad is not None:
            # grad += calc_zeeman_grad(self.Z_ad, spins.shape[0])
            Z, e_c = self.Z_ad['ampl'], np.asarray(self.Z_ad['dir'])
            gradient_module.zgrad(Z, e_c, grad)

        return grad
