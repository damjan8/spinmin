import numpy as np
from spinmin.units import R
import dyn_module


class SIB:

    """
    need to integrate this equation

    ds/dt = y ( -[s, b + f' + a [s, b + f']] ) / (M (1 + a^2) )

    s - unit vector
    y - electron gyromagnetic ration
    a - damping parameter
    M = g_i m m_b - length og magnetic moment in m_b (Bohr magneton),
    g_i - Factor Lande
    b - effective field: (- dE/ds)
    f' - random force

    <f' f'> = 2 D' * delta(dt), D' = a kbT M/y

    we will change time step dt to dh =  dt c =dt y / (M (1 + a^2))

    ds = dh (-[s, b + f'(h/c) + a [s, b]])
    <f' f'> = 2 D' delta(h/c) = 2 D' c delta(h) = 2 D delta(h)

    let f(h) = f'(h/c)

    So, we get the equation:

    ds = dh (-[s, b + f + a [s, b + f]])
    <f f> = 2 D delta(h),
    D = T a/(1 + a^2)
    dh = dt R / (m' (1 + a^2)).

    y/M = R / m',
    where R = y/ (m_b),
    ans m' = m g_i -length of magnetic moments (Bohr Magneton)

    Now, if we measure r in (1 / (fs meV))
    and fields in b ((- dE/ds)) in meV,
    then time step is in units of fs

    """

    def __init__(self, eq_type, alpha, dt, temp, damping_only):

        self.eq_type = eq_type
        self.temp = temp
        self.damping_only = damping_only
        self.alpha = alpha
        self.dt = dt

        if self.eq_type == 'LLG':
            self.h = dt / (1.0 + alpha**2.0)
        elif self.eq_type == 'LL':
            self.h = dt
        else:
            raise NotImplementedError
        if self.temp is not None:
            self.D = temp * alpha / (1.0 + alpha ** 2)
        else:
            self.D = None

        # R = y/m_b in units of (1 / (fs meV))
        # TODO: we also need to divide it
        #  by a length of magnetic moment
        #  we do it in update right now

        self.h *= R
        self.error = np.inf

    def update(self, ham):

        if ham.type == 'single_image':
            mm = ham.mm[0]
        else:
            mm = 1.0
        D = self.D
        if D is None:
            D = 0.0
            finit_temperature = False
        else:
            finit_temperature = True

        damping_only = self.damping_only
        h, alpha = self.h / mm, self.alpha

        predictor = True
        spins_old = ham.spins.copy()
        b = -ham.get_gradients()
        ksi = np.random.normal(size=3*spins_old.shape[0])
        self.error = dyn_module.sib_step(ham.spins, spins_old, b,
                                         ksi, alpha, h, D,
                                         damping_only,
                                         finit_temperature, predictor)
        predictor = False
        b = -ham.get_gradients()
        dyn_module.sib_step(ham.spins, spins_old, b,
                            ksi, alpha, h, D,
                            damping_only,
                            finit_temperature, predictor)
