import numpy as np
from spinmin.hamiltonian import SpinHamiltonian
from spinmin.utilities import plot_xy


class GNEB:

    def __init__(self, climbing_image=True,
                 spring='energy_weighted',
                 geodesic=True):
        """

        :param climbing_image: run climbing image gneb
        :param spring: constant or energy_weighted
        """
        self.geodesic = geodesic
        self.chain = []
        self.nos = None  # number of spins per image
        self.noi = 0  # number of images including end points
        self.spins = None  # spins of the all images
        self.tangent_topath = None
        self.spring_grad = None
        self.iters = 1
        self.CI = None  # Climbing image
        self.e_total = np.array([0.0])
        self.type = 'multi_images'
        self.climbing_image = climbing_image
        self.spring = spring

    def interpolate_chain(self, ham_i, ham_f, noi=7, rotaxis=None):
        """
        :param ham_i: the hamiltonian class of the first image
        :param ham_f: the hamiltonian class of the last image
        :param noi:  number of images including end points
        :return:
        """
        self.nos = len(ham_i.spins)
        self.noi = noi
        hamlist = [ham_i]
        for v in range(noi-2):
            hamlist.append(SpinHamiltonian(ham_i.atoms,
                                           ham_i.spins,
                                           ham_i.interactions))
        hamlist.append(ham_f)
        self.chain = hamlist

        self.interpolate(rotaxis=rotaxis)

    def interpolate(self, initial=0, final=-1, rotaxis=None):
        # Rodrigues' rotation formula
        spins_i = self.chain[initial].spins
        spins_f = self.chain[final].spins
        # find the angle between initial and final
        delta_omega = np.arccos(np.sum(spins_i * spins_f, axis=1)) / (self.noi-1)

        #  find the axis of rotation cross initial spins
        cross = np.cross(spins_i, spins_f)

        # if spins are (anti)parallel rodr. form. does not work
        # zero vector equals 1 where cross = 0 and 0 where cross != 0
        zero = np.sum(cross*cross, axis=1)
        zero[zero == 0] = 10
        zero[zero < 10] = 0
        zero[zero == 10] = 1
        zero = np.array([zero, zero, zero]).T

        np.random.seed(seed=7)
        if rotaxis is None:
            rotaxis = [0, 0, 1]
        temp_vec = np.repeat(np.array([rotaxis]), self.nos, axis=0) * zero

        cross += temp_vec - (temp_vec*spins_i)*spins_i

        norm = np.linalg.norm(cross, axis=1)
        # instead of dividing by zero make k=0
        norm[norm == 0] = np.inf
        k = cross / norm[:, np.newaxis]
        # this should always be normalized
        kcrossm_i = np.cross(k, spins_i)

        for v in range(1, self.noi-1):
            temp_spins = \
                spins_i * np.cos(v * delta_omega[:, np.newaxis]) + \
                kcrossm_i * np.sin(v * delta_omega[:, np.newaxis])

            # add some noise
            temp_spins += np.random.rand(self.nos, 3) * 0.0005
            norm = np.linalg.norm(temp_spins, axis=1)
            # instead of dividing by zero make k=0
            norm[norm == 0] = np.inf
            temp_spins /= norm[:, np.newaxis]

            self.chain[v].spins = temp_spins

        self.spins = \
            np.array([
                self.chain[i].spins
                for i in
                range(1, self.noi-1)]).reshape(
                (self.noi-2) * self.nos, 3)

    def get_tangent_topath(self):

        """"
        Get a matrix of vectors pointing in the tangent direction
        of the current path see
        Bessarab et al, Comput.Phys. Commun. 196, 335 (2015), App. A
        :return:
        """

        noi = self.noi
        nos = self.nos
        chain = self.chain
        M = self.chain
        self.tangent_topath = np.zeros(((noi-2)*nos, 3))

        # find the tangent for v = 1,...,noi-2 (all images except the first and last)
        self.e_total = E = np.zeros(noi)  # energy of each image
        for i in range(noi):
            E[i] = chain[i].get_energy()
        for v in range(1, noi - 1):

            Evmax = max(abs(E[v] - E[v - 1]), abs(E[v+1] - E[v]))
            Evmin = min(abs(E[v] - E[v - 1]), abs(E[v+1] - E[v]))
            if E[v] > E[v - 1]:
                if E[v] <= E[v + 1]:
                    t = M[v + 1].spins - M[v].spins
                else:
                    if E[v + 1] > E[v - 1]:
                        # tauplus * Evmax + tauminus * Evmin
                        t = (M[v + 1].spins - M[v].spins) * Evmax + \
                            (M[v].spins - M[v - 1].spins) * Evmin
                    else:
                        # tauplus * Evmin + tauminus * Evmax
                        t = (M[v + 1].spins - M[v].spins) * Evmin + \
                            (M[v].spins - M[v - 1].spins) * Evmax
            else:  # E[v] < E[v-1]
                if E[v] >= E[v + 1]:
                    # tauminus
                    t = M[v].spins - M[v - 1].spins
                else:
                    if E[v + 1] > E[v - 1]:
                        # tauplus * Evmax + tauminus * Evmin
                        t = (M[v + 1].spins - M[v].spins) * Evmax + \
                            (M[v].spins - M[v - 1].spins) * Evmin
                    else:
                        # tauplus * Evmin + tauminus * Evmax
                        t = (M[v + 1].spins - M[v].spins) * Evmin + \
                            (M[v].spins - M[v - 1].spins) * Evmax

            self.tangent_topath[(v-1)*self.nos:v*self.nos] = t

        self.tangent_topath = self.project_on_tangent_tosphere(self.tangent_topath)

        for v in range(self.noi-2):
            a = v * self.nos
            b = a + self.nos
            norm = np.linalg.norm(self.tangent_topath[a:b])
            if norm == 0:
                norm = np.inf
            self.tangent_topath[a:b] /= norm

    def project_on_tangent_tosphere(self, f, image=None):
        # if f is only for one image we must pass the image number
        if image is None:
            return f - np.sum(f*self.spins, axis=1)[:, np.newaxis]*self.spins
        else:
            x = self.nos * image
            y = self.nos * (image+1)
            return f - np.sum(f*self.spins[x:y], axis=1)[:, np.newaxis]*self.spins[x:y]

    def get_distance(self, spins1, spins2):
        if self.geodesic:
            # Vincety's formula
            dot = np.sum(spins1 * spins2, axis=1)
            cross = np.linalg.norm(np.cross(spins1, spins2), axis=1)
            return np.linalg.norm(np.arctan2(cross, dot))
        else:
            return np.linalg.norm(spins2-spins1)

    def get_spring_grad(self):
        """

        :return: updates self.spring_grad
        """

        if self.spring == 'energy_weighted':
            k_spring = [2.0] * (self.noi-1)

            E = [self.chain[i].get_energy() for i in range(self.noi)]
            E_ref = max(E[0], E[-1])
            E_max = max(E)
            dk = k_spring[0]/2
            for i in range(1, self.noi):
                E_i = max(E[i], E[i-1])
                if E_i > E_ref:
                    k_spring[i-1] -= dk*(E_max-E_i)/(E_max-E_ref)
                else:
                    k_spring[i-1] -= dk
        elif self.spring == 'constant':
            k_spring = [1.0] * (self.noi-1)
        elif type(self.spring) == float:
            k_spring = [self.spring]*(self.noi-1)
        else:
            raise ValueError('Specify spring variable')

        M = self.chain
        grad = np.zeros_like(self.tangent_topath)
        # spring grad = 0 at the first and last image
        for v in range(self.noi - 2):
            if self.CI is not None and (v+1) == self.CI:
                continue
            a = v * self.nos
            b = a + self.nos
            L1 = self.get_distance(M[v + 2].spins, M[v + 1].spins)
            L2 = self.get_distance(M[v + 1].spins, M[v].spins)
            grad[a:b] = (k_spring[v]*L2 - k_spring[v+1]*L1) * self.tangent_topath[a:b]
        self.spring_grad = grad

    def get_gradients(self, spring=True):
        """
        Gets gradients from hamiltonian class and the spring gradient (optional)
        and projects it on the tangent to the energy path which is also
        tangent to a unit sphere around each spin.

        :param spring: True/False if we want spring force or not
        :return: gradient with or without imaginary springs
        """
        # make sure the spins in the hamiltonian are up to date
        for v in range(self.noi-2):
            a = v * self.nos
            b = a + self.nos
            self.chain[v+1].spins = self.spins[a:b]

        # add the energy gradient
        eff_field = np.zeros([(self.noi-2)*self.nos, 3])
        for v in range(self.noi-2):
            eff_field[v*self.nos:(v+1)*self.nos] = self.chain[v+1].get_gradients()

        # pass spring=False if we don't want the spring constant
        if spring:
            # first we need to update
            # tangent_topath and then spring_grad
            self.get_tangent_topath()
            self.get_spring_grad()
            # subtract the tangent to the path component
            for v in range(self.noi-2):
                a = v * self.nos
                b = a + self.nos
                x = np.sum(eff_field[a:b] * self.tangent_topath[a:b])

                # For the climbing image we go against
                # the gradient, hence multiply by 2
                if self.CI is not None and (v + 1) == self.CI:
                    x *= 2
                eff_field[a:b] = \
                    eff_field[a:b] - x * self.tangent_topath[a:b]

            # add the spring gradient
            eff_field += self.spring_grad

        return eff_field

    def plot_chain(self, name='Figure'):

        """
        plot every image but it is valid only for a monolayer

        :param name: name of images plotted with the number of the image following
        """

        for i in range(self.noi):
            plot_xy(self.chain[i].atoms, self.chain[i].spins, name=name + str(i))

    def plot_interpolation(self, name='interpolation', title=None):
        if self.tangent_topath is None:
            self.get_tangent_topath()
        import matplotlib.pyplot as plt
        a = []
        b = []
        c = [0]

        cc = np.sum(self.get_gradients(spring=False)*self.tangent_topath, axis=1)
        for i in range(self.noi):
            temp = sum(cc[i*self.nos:(i+1)*self.nos])
            c.append(temp)
        c.append(0)
        d = [self.chain[v].e_total for v in range(self.noi)]
        M = self.chain
        L = [0]
        for v in range(1, self.noi):
            L.append(L[v-1] + self.get_distance(M[v].spins, M[v-1].spins))

        for v in range(self.noi-1):

            l = L[v+1] - L[v]
            a.append((c[v+1]+c[v])/l**2 - 2*(d[v+1]-d[v])/l**3)
            b.append(-(c[v+1]+2*c[v])/l + 3*(d[v+1]-d[v])/l**2)

        interval = np.linspace(0, L[-1], 100)
        plot = []
        for x in interval:
            for i in range(len(L) - 1):
                if L[i] <= x <= L[i+1]:
                    v = i
                    break
            xL = x-L[v]
            plot.append(a[v]*xL**3 + b[v]*xL**2 + c[v]*xL + d[v])

        if title is None:
            title1 = "energy between states: %f" % (d[0]-d[-1])
            title2 = "energy barrier: %f" % (max(d)-d[0])
            title = title1 + '\n' + title2

        plt.figure()
        plot = [plot[i]-d[0] for i in range(len(plot))]
        plt.plot(interval, plot)
        d = [d[i]-d[0] for i in range(len(d))]
        plt.plot(L, d, "r.")
        plt.ylabel("$E - E_{initial}$")
        plt.xlabel("reaction coordinates")
        plt.axis("auto")
        plt.title(title)
        plt.savefig(name, bbox_inches='tight', pad_inches=.1)

    def get_energy(self):
        return self.e_total.sum()

    def read(self, atoms, interactions):
        """
        initialises the gneb class according to the list of atoms
        :param atoms: list of atom objects from ase
        :param interactions: interaction for the hamiltonian
        """
        hamlist = []
        for atom in atoms:
            hamlist.append(SpinHamiltonian(atom,
                                           atom.get_initial_magnetic_moments(),
                                           interactions))

        self.nos = len(hamlist[0].spins)
        self.noi = len(atoms)

        self.chain = hamlist
        self.spins = \
            np.array([
                self.chain[i].spins
                for i in
                range(1, self.noi - 1)]).reshape(
                (self.noi - 2) * self.nos, 3)

    def write(self, filename='Path.traj'):
        from ase import io

        atoms = []
        atom = self.chain[0].atoms
        for v in range(self.noi):
            atom.set_initial_magnetic_moments(self.chain[v].spins)
            atoms.append(atom.copy())
        io.write(filename, atoms)

    def set_climbing_image(self):
        if len(self.e_total) == 1:
            self.e_total = np.zeros(self.noi)
            for i in range(self.noi):
                self.e_total[i] = self.chain[i].get_energy()

        self.CI = np.where(self.e_total == max(self.e_total[1:-1]))[0][0]
        print('climbing image is ' + str(self.CI))

