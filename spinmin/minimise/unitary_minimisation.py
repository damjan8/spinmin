import numpy as np
from . import search_direction, line_search_algorithm
import uo_module as uo


class UnitaryOptimisation:

    def __init__(self, hamiltonian,
                 searchdir_algo='LBFGS2',
                 linesearch_algo=None,
                 update_ref_spins_counter=50,
                 convergence=1.0e-5,
                 max_iter=100000,
                 approx_grad=False):

        if str(searchdir_algo) in ['LBFGS2', 'QuickMin']:
            update_ref_spins_counter = 10000000000
            approx_grad = True

        if linesearch_algo is None:
            if hamiltonian.type == 'multi_images':
                linesearch_algo = 'CutOff'
            else:
                linesearch_algo = 'SwcAwc'

        nspins = hamiltonian.spins.shape[0]
        self.ham = hamiltonian

        self.sda = searchdir_algo
        self.lsa = linesearch_algo
        # skew-hermitian matrix to be exponented:
        self.a_vec = np.zeros(shape=(3 * nspins))
        # gradient matrix:
        self.g_vec = np.zeros(shape=(3 * nspins))
        self.s_ref = hamiltonian.spins.copy()

        self.alpha = 1.0  # step length
        self.phi_2i = [None, None]  # energy at last two iterations
        self.der_phi_2i = [None, None]  # energy gradient w.r.t. alpha

        self.iters = 1
        self.eg_counter = 0
        self.global_iters = 0

        # choose search direction and line search algorithm
        self.search_direction = search_direction(self.sda)
        #
        self.line_search = \
            line_search_algorithm(self.lsa,
                                  self.evaluate_phi_and_der_phi,
                                  self.sda)

        self.update_ref_spins_counter = update_ref_spins_counter

        self.error = np.inf
        self.convergence = convergence
        self.max_iter = max_iter
        self.approx_grad = approx_grad

    def evaluate_phi_and_der_phi(self, a_vec, p_vec, alpha,
                                 ham, s_ref, phi=None, g_vec=None):
        """
        phi = f(x_k + alpha_k*p_k)
        der_phi = \grad f(x_k + alpha_k*p_k) \cdot p_k
        :return:  phi, der_phi # floats
        """
        if phi is None or g_vec is None:
            x_vec = a_vec + alpha * p_vec
            phi, g_vec = self.get_energy_and_gradients(x_vec, ham,
                                                       s_ref)
            del x_vec
        else:
            pass

        der_phi = np.dot(g_vec, p_vec)

        return phi, der_phi, g_vec

    def get_energy_and_gradients(self, a_vec, ham, s_ref):

        self.eg_counter += 1

        self.rotate_all_spins(ham.spins, s_ref, a_vec)

        energy = ham.get_energy()
        eff_field = ham.get_gradients()

        # calculate gradients approximately
        grad = np.zeros_like(eff_field)

        if self.approx_grad:
            self.error = uo.get_approx_gradient_vector(a_vec, ham.spins,
                                                       eff_field, grad)
        else:
            self.error = uo.get_gradient_vector(a_vec, ham.spins,
                                                eff_field, grad)

        grad = grad.reshape(3 * grad.shape[0])
        return energy, grad

    def iterate(self):

        if str(self.search_direction) in ['LBFGS2', 'QuickMin']:
            self.s_ref = self.ham.spins.copy()
            if self.iters == 1:
                self.a_vec = np.zeros_like(self.a_vec)
        else:
            self.update_ref_spins(self.ham)

        phi_2i = self.phi_2i
        der_phi_2i = self.der_phi_2i
        a_vec = self.a_vec
        ham = self.ham
        s_ref = self.s_ref
        alpha = self.alpha

        if self.iters == 1:
            phi_2i[0], g_vec = self.get_energy_and_gradients(a_vec,
                                                             ham,
                                                             s_ref)
        else:
            g_vec = self.g_vec

        p_vec = self.search_direction.update_data(a_vec, g_vec)
        der_phi_2i[0] = np.dot(g_vec, p_vec)

        if str(self.search_direction) in ['LBFGS2', 'QuickMin']:
            a_vec = np.zeros_like(a_vec)

        alpha, phi_alpha, der_phi_alpha, g_vec = \
            self.line_search.step_length_update(a_vec, p_vec, ham, s_ref,
                                                phi_0=phi_2i[0],
                                                der_phi_0=der_phi_2i[0],
                                                phi_old=phi_2i[1],
                                                der_phi_old=der_phi_2i[1],
                                                alpha_max=5.0,
                                                alpha_old=alpha)

        # calculate new matrices for optimal step length

        if str(self.search_direction) in ['LBFGS2', 'QuickMin']:
            del a_vec
            self.a_vec = alpha * p_vec
        else:
            a_vec += alpha * p_vec

        self.alpha = alpha
        self.g_vec = g_vec.copy()
        # and 'shift' phi, der_phi for the next iteration
        phi_2i[1], der_phi_2i[1] = phi_2i[0], der_phi_2i[0]
        phi_2i[0], der_phi_2i[0] = phi_alpha, der_phi_alpha,

        self.iters += 1
        self.global_iters += 1
        if self.ham.type == 'multi_images':
            self.ham.iters += 1

    def run(self):
        while True:
            self.iterate()

            condition1 = self.error < self.convergence
            condition2 = self.global_iters == self.max_iter

            if self.global_iters % 50 == 0:
                print(self.global_iters, self.ham.e_total,
                      self.error)

            if self.ham.type == 'multi_images':
                if self.ham.climbing_image and condition1 and self.ham.CI is None:
                    self.ham.set_climbing_image()
                    continue
                if self.ham.climbing_image and self.global_iters % 200 == 0:
                    self.ham.set_climbing_image()

            if condition1 or condition2:
                if self.ham.type == 'single_image':
                    self.ham.atoms.set_initial_magnetic_moments(self.ham.spins)

                print(self.global_iters, self.ham.e_total,
                      self.error)
                break

    def rotate_all_spins(self, spins, s_ref, a_vec):
        uo.rotate_all_spins(a_vec, s_ref, spins)
        # for i, s in enumerate(s_ref):
        #     u_mat = skh2u(a_vec[3*i:3*i+3])
        #     spins[i] = np.dot(s, u_mat)

    def update_ref_spins(self, ham):
        counter = self.update_ref_spins_counter
        if self.iters % counter == 0 and self.iters > 1:
            self.s_ref = ham.spins.copy()
            self.a_vec = np.zeros_like(self.a_vec)

            # choose search direction and line search algorithm
            self.search_direction = search_direction(self.sda)
            #
            self.line_search = \
                line_search_algorithm(self.lsa,
                                      self.evaluate_phi_and_der_phi,
                                      self.sda)

            # self.alpha = 1.0
            # self.phi_2i = [None, None]
            # self.der_phi_2i = [None, None]
            self.iters = 1

    def calc_numerical(self, property='check_gradients'):
        spins = self.ham.spins
        s_ref = self.s_ref
        a_vec = self.a_vec

        self.rotate_all_spins(spins, s_ref, a_vec)
        s_ref = spins.copy()
        a_vec = np.zeros_like(a_vec)
        eps = 1.0e-3
        h = [eps, -eps]
        coief = [1.0, -1.0]

        if property == 'check_gradients':
            g_num = np.zeros_like(a_vec)
            g_an = self.get_energy_and_gradients(a_vec, self.ham,
                                                 s_ref)[1]
            value = 0
        elif property == 'evals_evecs_hess':
            hess = np.zeros(shape=(a_vec.shape[0], a_vec.shape[0]))
            value = 1
        else:
            ValueError('check_gradients or evals_evecs_hess')

        for i in range(a_vec.shape[0]):
            print(i)
            a = a_vec[i]
            for l in range(2):
                a_vec[i] = a + h[l]

                e_h = self.get_energy_and_gradients(a_vec, self.ham,
                                                    s_ref)[value]

                if property == 'check_gradients':
                    g_num[i] += e_h * coief[l]
                elif property == 'evals_evecs_hess':
                    hess[i, :] += e_h * coief[l]

            if property == 'check_gradients':
                g_num[i] *= 1.0 / (2.0 * eps)
            elif property == 'evals_evecs_hess':
                hess[i, :] *= 1.0 / (2.0 * eps)

            a_vec[i] = a

        if property == 'check_gradients':
            return g_num, g_an
        elif property == 'evals_evecs_hess':
            return np.linalg.eigh(hess)
