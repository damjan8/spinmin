from ase.build import bcc100
from spinmin.hamiltonian import SpinHamiltonian
from spinmin.minimise.unitary_minimisation import UnitaryOptimisation
from spinmin.utilities import random_spins, plot_xy

atoms = bcc100('Fe', a=2.856, orthogonal=True, size=(20, 20, 1))
spins = random_spins(len(atoms), seed=7)

ham = SpinHamiltonian(atoms, spins,
                      interactions={'J': 10.0,
                                    'DM': 5.0,
                                    'Z_ad': {'ampl': 2.0,
                                             'dir': [0.0, 0.0, 1.0]},
                                    'r_c': 1.45})

opt = UnitaryOptimisation(ham)
opt.run()
plot_xy(atoms, spins)