from ase.build import hcp0001
from spinmin.utilities import theta_function, plot_xy, from_mT_to_meV
from spinmin.hamiltonian import SpinHamiltonian
from spinmin.minimise.unitary_minimisation import UnitaryOptimisation
from ase import io
size = 50
a = 2.51

# Choose atom struct.
atoms = hcp0001('Fe', a=a, orthogonal=True, size=(size, size, 1))
# define interaction
interactions={'J': 29.0,
              'DM': {'ampl': 1.5,
                     'dir': [0.0, 0.0, 1.0]},
              # 'Z_ad': {'ampl': from_mT_to_meV(2.1, 250),
              #          'dir': [0.0, 0.0, 1.0]},
              'K_ad': {'ampl': 0.293,
                       'dir': [0.0, 0.0, 1.0]},
              'r_c': a/2+0.1}

# initial spin_configuration
spins = theta_function(atoms, 6*a)
ham = SpinHamiltonian(atoms, spins, interactions)
opt = UnitaryOptimisation(ham)
opt.run()
plot_xy(atoms, spins, 'init_state')
io.write('init_state.traj', atoms)
