from ase.build import hcp0001, fcc111, bcc110
from spinmin.utilities import theta_function, plot_xy, from_mT_to_meV, collinear_state
from spinmin.hamiltonian import SpinHamiltonian
from spinmin.minimise.unitary_minimisation import UnitaryOptimisation
from ase import io

a = 2.87

# Choose atom struct.
atoms = bcc110('Fe', a=a, orthogonal=True, size=(50, 20, 1))
# set the periodic boundary condition off
atoms.set_pbc(False)

# define interaction
interactions = {'J': 25.6,
                'K_ad': {'ampl': 1.2,
                         'dir': [0.0, 1.0, 0.0]},
                'r_c': a/2 + 0.1}

# initial spin_configuration to y
spins = collinear_state(len(atoms), along='y')

# initialise the hamiltonian
ham = SpinHamiltonian(atoms, spins, interactions)

# initialises the Unitary Optimisation and runs the calculation for the minimum
opt = UnitaryOptimisation(ham, searchdir_algo='LBFGS2', linesearch_algo='CutOff')
opt.run()

# plots the initial spin state and saves the Atom class for the MEP calculation
plot_xy(atoms, spins, 'init_state_flat')
io.write('init_state_flat.traj', atoms)
