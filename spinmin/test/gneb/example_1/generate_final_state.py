from ase.build import hcp0001, fcc111, fcc110, bcc110
from spinmin.utilities import collinear_state, plot_xy, from_mT_to_meV
from spinmin.hamiltonian import SpinHamiltonian
from spinmin.minimise.unitary_minimisation import UnitaryOptimisation
from ase import io
size = 100
a = 2.87


# Choose atom struct.
atoms = bcc110('Fe', a=a, orthogonal=True, size=(size, size, 1))

# define interaction
interactions = {'J': 12.88,
                'DM': {'ampl': 2.4,
                       'dir': [0.0, 0.0, 1.0]},
                'Z_ad': {'ampl': from_mT_to_meV(3, 1500),
                         'dir': [0.0, 0.0, 1.0]},
                'K_ad': {'ampl': 0.8,
                         'dir': [0.0, 0.0, 1.0]},
                'r_c': a/2 + 0.1}

# initial spin_configuration
spins = collinear_state(len(atoms), along='z')

# initialise the hamiltonian
ham = SpinHamiltonian(atoms, spins, interactions)

# initialises the Unitary Optimisation and runs the calculation for the minimum
opt = UnitaryOptimisation(ham, searchdir_algo='LBFGS2', linesearch_algo='CutOff')
opt.run()

# plots the final spin state and saves the Atom class for the MEP calculation
plot_xy(atoms, spins, 'final_state_skyrm')
io.write('final_state_skyrm.traj', atoms)
