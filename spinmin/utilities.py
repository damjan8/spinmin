import numpy as np
from spinmin.units import BohrMagn


def skh2u(upp_tr, real=True):

    if type(upp_tr) == list:
        upp_tr = np.asarray(upp_tr)
    if np.allclose(upp_tr, np.zeros_like(upp_tr)):
        return np.eye(3)

    evals, evec = egdecomp_skhm(upp_tr)

    u_mat = (evec * np.exp(evals)) @ evec.T.conj()

    if real is True:
        u_mat = u_mat.real

    return u_mat


def egdecomp_skhm(upp_tr):

    if type(upp_tr) == list:
        upp_tr = np.asarray(upp_tr)
    if np.allclose(upp_tr, np.zeros_like(upp_tr)):
        return np.array([0.0, 0.0, 0.0]), np.eye(3)

    x = np.sqrt(np.dot(upp_tr, upp_tr))
    a = upp_tr[0]
    b = upp_tr[1]
    c = upp_tr[2]

    v1 = np.array([c/x, -b/x, a/x])
    ac = np.sqrt(a**2 + c**2)

    if ac < 1.0e-12:
        if b < 0.0:
            v2 = np.array([-1.0j, 0.0, 1.0]) / np.sqrt(2.0)
        else:
            v2 = np.array([1.0j, 0.0, 1.0]) / np.sqrt(2.0)
    else:
        v2 = np.array([b * c + 1.0j * a * x,
                       a**2 + c**2,
                       a * b - 1.0j * c * x]) / \
             (x * np.sqrt(2.0) * ac)

    v3 = v2.conj()

    return np.array([0.0, -1.0j*x, 1.0j*x]), \
           np.array([v1, v2, v3]).T


def get_grad(upp_tr, m_uptr):

    m_cc = np.zeros(shape=(3, 3))
    m_cc[([1, 2, 0], [2, 0, 1])] = m_uptr
    m_cc += -m_cc.T

    evals, evec = egdecomp_skhm(upp_tr)

    grad = np.dot(evec.T.conj(), np.dot(m_cc, evec))
    grad = grad * D_matrix(1.0j * evals)
    grad = np.dot(evec, np.dot(grad, evec.T.conj()))
    return grad[([0, 0, 1], [1, 2, 2])].real


def D_matrix(omega):

    m = omega.shape[0]
    u_m = np.ones(shape=(m, m))

    u_m = omega[:, np.newaxis] * u_m - omega * u_m

    with np.errstate(divide='ignore', invalid='ignore'):
        u_m = 1.0j * np.divide(np.exp(-1.0j * u_m) - 1.0, u_m)

    u_m[np.isnan(u_m)] = 1.0
    u_m[np.isinf(u_m)] = 1.0

    return u_m


def random_spins(n_atoms, length=1.0, seed=None):

    if seed is not None:
        assert type(seed) == int
        np.random.seed(seed)
    spins = np.zeros(shape=(n_atoms, 3))
    for i, s in enumerate(spins):
        z = np.random.uniform(-1.0, 1.0)
        phi = np.random.uniform(0.0, 2.0 * np.pi)
        x = np.sqrt(1.0 - z ** 2) * np.cos(phi)
        y = np.sqrt(1.0 - z ** 2) * np.sin(phi)
        spins[i] = length * np.array([x, y, z])
    return spins


def plot_xy(atoms, spins, name='Figure'):

    import matplotlib.pyplot as plt

    pos = atoms.positions
    plt.figure(figsize=(8, 6))
    plt.axis('equal')
    plt.xlabel(r'x coordinate ($\AA$)')
    plt.ylabel(r'y coordinate ($\AA$)')
    plt.quiver(pos[:, 0], pos[:, 1], spins[:, 0], spins[:, 1], spins[:, 2],
               scale_units="inches", cmap="jet",
               scale=15, width=0.015, pivot="mid")
    cbar = plt.colorbar()
    cbar.ax.set_ylabel(r'$S_z$', rotation=0)
    plt.clim(-1, 1)

    plt.savefig(name + '.png', bbox_inches='tight', pad_inches=.1)


def read_from_txt(name, n_atoms):

    spins = np.zeros(shape=(n_atoms, 3))
    f = open(name + '.txt', 'r')
    i = 0
    for l in f:
        x = l.rstrip('\n')
        x = x.split()
        spins[i] = np.array([float(x[0]), float(x[1]), float(x[2])])
        i += 1
    f.close()

    return spins


def collinear_state(n_atoms, type='f', along='z'):

    spins = np.zeros(shape=(n_atoms, 3))
    if along == 'x':
        d = 0
    elif along == 'y':
        d = 1
    elif along == 'z':
        d = 2
    else:
        raise ValueError('along can be x, y or z')

    if type == 'f':
        spins[:, d] = 1.0
    elif type == 'af':
        spins[::2, d] = 1.0
        spins[1::2, d] = -1.0

    return spins


def from_mT_to_meV(magmom, H):

    return magmom * BohrMagn * H


def from_Kelv_to_meV(T):
    return T * 8.621738e-2


def theta_function(atoms, r, r0=None, length=1.0):

    """
    ferromagnetic background along z direction
    :param atoms:
    :param r: radius theta function
    :param r0: the center of theta function
    :param length: length of magnetic moments
    :return:
    """

    #TODO: does it work on a boundary?
    assert length > 0.0
    pos = atoms.positions
    if r0 is None:
        r0 = atoms.get_center_of_mass()

    d = [np.linalg.norm(r0-item) for item in pos]
    spins=np.array([[0.0, 0.0, length]] * len(atoms))
    spins[[i for i in range(len(spins))
           if d[i] < r]]=np.array([0.0,0.0,-length])

    return spins


def save_csv(atoms, spins, name='magnetization.csv'):
    import csv
    pos = atoms.positions
    with open(name, 'w', newline='') as csvfile:

        spamwriter = csv.writer(
            csvfile, delimiter=' ', quotechar='|',
            quoting=csv.QUOTE_MINIMAL)

        spamwriter.writerow(['N','x','y','z','s_x','s_y','s_z'])
        for i in range(len(pos)):
            spamwriter.writerow([i]+pos[i].tolist()+spins[i].tolist())


def sk_radius(atoms, spins):

    """
    assume that ferromagnetic background points to z-direction

    :param atoms:
    :param spins:
    :return: Effective radius of skyrmion using integral
    """
    # FIXME: skyrmion must be in around of the cell centre
    #  works only for one skyrmion

    pos = atoms.positions
    r0 = atoms.get_center_of_mass()
    dr0 = np.linalg.norm(r0)*0.5
    c_c = np.average(
        pos, axis=0,
        weights=[np.maximum(0.0,-spins[item,2]) *
                 float(np.linalg.norm(r0-pos[item])<dr0)
                 for item in range(len(atoms))])
    d = [np.linalg.norm(c_c-item) for item in pos]
    sortedz = [x for _,x in sorted(zip(d, enumerate(spins[:,2])))]
    s = 0.0
    i = 0
    while s <= 0.0:
        s += sortedz[i][1]
        i += 1
    r_eff = d[sortedz[i][0]]

    print('Effective radius of the skyrmion = ', r_eff)

    return r_eff


def AtoJ(a, A):
    """

    :param a: effective size of primitive cell in Ang
    :param A: micromagnetic exchange J/m
    :return: J in meV
    """

    return a * A * 6.24150974e11


def K_micro_toK_atom(a, K):
    """

    :param a: effective size of primitive cell in Ang
    :param K: J/m^3
    :return:
    """

    return a**3.0 * K * 6.24150974e18 * 1000.0 / (1.0e30)

def D_micro_toD_atom(a, D):
    """

    :param a: effective size of primitive cell in Ang
    :param D: J/m^2
    :return:
    """

    return a**2.0 * D * 6.24150974e18 * 1000.0 / (1.0e20)


def Mstomu(a, Ms):
    """
    magnetic moment in units of Bohr magneton of primitive cell
    :param a: effective size of primitive cell in Ang
    :param Ms: A/m
    :return:
    """

    return 6.24150974e-12 * Ms * a**3.0 / BohrMagn