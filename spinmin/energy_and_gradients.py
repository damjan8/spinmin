import numpy as np


def calc_heis_energy(J, spins, nbl_one_way):

    n_atoms = spins.shape[0]
    energy = 0.0

    for i in range(n_atoms):
        nl_indices = nbl_one_way.get_neighbors(i)[0]
        for j in nl_indices:
            energy += np.dot(spins[i], spins[j])

    return -1.0 * J * energy


def calc_dm_energy(D, atoms, spins, nbl_one_way):

    n_atoms = spins.shape[0]
    energy = 0.0

    for i in range(n_atoms):
        nl_indices, offsets = nbl_one_way.get_neighbors(i)
        for j, offset in zip(nl_indices, offsets):
            cross_ij = np.cross(spins[i], spins[j])
            r_ij = atoms[i].position - \
                   (atoms[j].position +
                    np.dot(offset, atoms.get_cell()))
            r_ij /= np.sqrt(np.dot(r_ij, r_ij))
            energy += np.dot(r_ij, cross_ij)

    return -1.0 * D * energy


def calc_anisotropy_energy(K_ad, spins):

    K, e_an = K_ad['ampl'], K_ad['dir']
    energy = 0.0
    n_atoms = spins.shape[0]

    for i in range(n_atoms):
        energy += np.dot(e_an, spins[i])**2

    return -K * energy


def calc_zeeman_energy(Z_ad, spins):

    Z, b_vec = Z_ad['ampl'], np.asarray(Z_ad['dir'])
    energy = 0.0
    n_atoms = spins.shape[0]

    for i in range(n_atoms):
        energy += np.dot(b_vec, spins[i])

    return - Z * energy


def calc_heis_grad(J, spins, nbl_both_way):

    n_atoms = spins.shape[0]
    grad = np.zeros(shape=(n_atoms, 3))

    for i in range(n_atoms):
        nl_indices = nbl_both_way.get_neighbors(i)[0]
        for j in nl_indices:
            grad[i] += spins[j]

    return -1.0 * J * grad


def calc_dm_grad(D, atoms, spins, nbl_both_way):

    n_atoms = spins.shape[0]
    grad = np.zeros(shape=(n_atoms, 3))

    for i in range(n_atoms):
        nl_indices, offsets = nbl_both_way.get_neighbors(i)
        for j, offset in zip(nl_indices, offsets):
            r_ij = atoms[i].position - \
                   (atoms[j].position +
                    np.dot(offset, atoms.get_cell()))
            r_ij /= np.sqrt(np.dot(r_ij, r_ij))
            grad[i] += np.cross(spins[j], r_ij)

    return -1.0 * D * grad


def calc_anisotropy_grad(K_ad, spins):

    K, e_an = K_ad['ampl'], np.asarray(K_ad['dir'])
    n_atoms = spins.shape[0]
    grad = np.zeros(shape=(n_atoms, 3))

    for i in range(n_atoms):
        grad[i] += 2.0 * np.dot(e_an, spins[i]) * e_an

    return -K * grad


def calc_zeeman_grad(Z_ad, n_atoms):

    Z, b_vec = Z_ad['ampl'], Z_ad['dir']
    grad = np.zeros(shape=(n_atoms, 3))

    for i in range(n_atoms):
        grad[i] += b_vec

    return -Z * grad
