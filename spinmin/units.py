EGM = 1.760859644e11  # electron gyromagnetic ration 1 / (s T)
BohrMagn = 5.7883818012e-5  # ev/T, Bohr Magneton
C_0 = 5.368573255301166e-05 # ev/A**3,  m_o * m_b**2 / 4pi

R = EGM / BohrMagn * 1.0e-16 * 1.0e-3  # (1 / (fs meV))